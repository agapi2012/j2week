﻿create database qlbh;
use qlbh;
CREATE TABLE `giay` (
  `magiay` int(11) ,
  `maloai` varchar(30) ,
  `tengiay` nvarchar(50) ,
  `dongia` int(11) ,
  `hinh` varchar(50),
  `mota` nvarchar(1000) 
) ;

--
-- Dumping data for table `hoa`
--

INSERT INTO `giay` (`magiay`, `maloai`, `tengiay`, `dongia`, `hinh`, `mota`) VALUES
(1, 'ADIDAS', N'GIÀY ADIDAS ALPHABOUNCE INSTINCT', 3690000, 'G1.jpg', N'Giày thể thao cao cấp Adidas.'),
(2, 'ADIDAS', N'GIÀY ADIDAS Stansmith ', 2200000, 'G2.jpg', N'Giày thể thao cao cấp Adidas'),
(3, 'ADIDAS', N'GIÀY ADIDAS TOP TEN MID PC', 2700000, 'G3.jpg', N'Giày thể thao cao cấp Adidas'),
(4, 'ADIDAS', N'GIÀY ADIZERO BOSTON BOOST 6', 5000000, 'G4.jpg', N'Giày thể thao cao cấp Adidas'),
(5, 'ADIDAS', N'GIÀY ADIDAS DURAMO 8 ', 5300000, 'G5.jpg', N'Giày thể thao cao cấp Adidas'),
(6, 'NIKE', N'GIÀY NIKE METCON 4 PREMIUM XANH CAMO', 6500000, 'G6.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(7, 'NIKE', N'GIÀY NIKE AIR MAX 94', 3500000, 'G7.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(8, 'NIKE', N'GIÀY NIKE AIR MAX 94', 7200000, 'G8.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(9, 'NIKE', N'GIÀY NIKE AIR MAX 94', 3600000, 'G9.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(10, 'NIKE', N'GIÀY NIKE DUALTONE RACER SE', 5500000, 'G10.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(11, 'BALENCIAGA', N'GIÀY balenciaga track 3.0 yellow', 18000000, 'G11.jpg', N'Giày cao cấp Balencia.'),
(12, 'BALENCIAGA', N'GIÀY Balenciaga Track 3.0', 21000000, 'G12.jpg', N'Giày cao cấp Balencia.'),
(13, 'BALENCIAGA', N'GiÀY Balenciaga Tripper S Red Black', 24000000, 'G13.jpg', N'Giày cao cấp Balencia.'),
(14, 'BALENCIAGA', N'GiÀY Balenciaga Tripper S Red White', 24500000, 'G14.jpg', N'Giày cao cấp Balencia.'),
(15, 'BALENCIAGA', N'GiÀY Balenciaga Speed Trainer Balck White', 21500000, 'G15.jpg', N'Giày cao cấp Balencia.');


CREATE TABLE `Jeans` (
  `majean` int(11) ,
  `maloai` varchar(30) ,
  `tenjean` nvarchar(50) ,
  `dongia` int(11) ,
  `hinh` varchar(50) ,
  `mota` nvarchar(1000) 
);

INSERT INTO `jeans` (`majean`, `maloai`, `tenjean`, `dongia`, `hinh`, `mota`) VALUES
(1, 1, N'Jean xanh Kimaru D20', 380000, 'jean1.jpg', N'Quần jean cao cấp màu xanh kimaru.');

INSERT INTO `jeans` (`majean`, `maloai`, `tenjean`, `dongia`, `hinh`, `mota`) VALUES
(2, 'JeanKimaru', N'Jean Nam Kimaru D218', 360000, 'jean2.jpg', N'Quần jean cao cấp màu đen kimaru.'),
(3,'JeanKimaru', N'Jean xanh Nam Kimaru D19', 380000, 'jean3.jpg', N'Quần jean cao cấp màu xanh kimaru.'),
(4, 'JeanKimaru', N'Jean xanh bạc  Kimaru D22', 380000, 'jean4.jpg', N'Quần jean cao cấp màu xanh nhạt kimaru.'),
(5, 'Jean Nostyle', N'Jean Nam Nostyle D45', 400000, 'jean5.jpg', N'Quần jean cao cấp Nostyle D45.'),
(6, 'Jean Nostyle', N'Jean Nam Nostyle A23', 400000, 'jean6.jpg', N'Quần jean cao cấp Nostyle A43.'),
(7, 'Jean Nostyle', N'Jean Nam Nostyle A25', 400000, 'jean7.jpg', N'Quần jean cao cấp Nostyle A25.'),
(8, 'Jean Nostyle', N'Jean Nam Nostyle A39', 400000, 'jean8.jpg', N'Quần jean cao cấp Nostule A39.'),
(9, 'Jean QK2HD', N'Jean Nam QK2HD', 999999, 'jean9.jpg', N'Quần jean phiên bản đặc biệt của hãng J2W.'),
(10, 'Jean QK2HD', N'Jean Nam KSH2', 500000, 'jean10.jpg', N'Sản phẩm đầu hè của J2W.');


CREATE TABLE `Tshirt` (
  `maao` int(11) ,
  `maloai` varchar(30) ,
  `tenao` nvarchar(50) ,
  `dongia` int(11) ,
  `hinh` varchar(50) ,
  `mota` nvarchar(1000) 
);

INSERT INTO `tshirt` (`maao`, `maloai`, `tenao`, `dongia`, `hinh`, `mota`) VALUES
(1, 'Ao Nostyle', N'Thun Nam Nostyle cổ trụ N01', 300000, 'T1.jpg', N'Áo thun cổ trụ cao cấp Nostyle N01.'),
(2, 'Ao Nostyle', N'Thun Nam Nostyle Essential ', 300000, 'T2.jpg', N'Áo thun nostyle chất liệu vải thoáng mát.'),
(3, 'Ao Nostyle', N'Thun Nam Nostyle Colaboss', 300000, 'T3.jpg', N'Thun Nam Nostyle Colaboss siêu mỏng.'),
(4, 'Ao Nostyle', N'Áo thun dài tay N1', 300000, 'T4.jpg', N'Áo dài tay thun mỏng cho mùa hè.'),
(5, 'Ao Basic', N'Áo thun cổ trụ M01', 270000, 'T5.jpg', N'Mát mẽ, sang trọng cho ngày hè.'),
(6, 'Ao Basic', N'Áo thun basic đen B01', 270000, 'T6.jpg', N'Chất liệu vải thấm hút tốt, thiết kế đơn giản nam tính.'),
(7, 'Ao Basic', N'Áo thun basic B02', 270000, 'T7.jpg', N'Thiết kế đơn giản, chất liệu thoáng mát cho ngày hè.'),
(8,'Ao L3', N'Áo thun tay ngắn Ak3', 255000, 'T8.jpg', N'Thiết kế đẹp mắt, năng động.'),
(9, 'Ao L3', N'Áo thun nam bassic N05', 255000, 'T9.jpg', N'Đơn giản. mạnh mẽ.'),
(10, 'Ao L3', N'Áo thun cổ trụ S04', 255000, 'T10.jpg', N'Áo thun cổ trụ hiện đại, năng động.');


CREATE TABLE `product` (
  `masp` int(11) ,
  `maloai` varchar(30) ,
  `tensp` nvarchar(50) ,
  `dongia` int(11) ,
  `hinh` varchar(50),
  `mota` nvarchar(1000) 
) ;

INSERT INTO `product` (`masp`, `maloai`, `tensp`, `dongia`, `hinh`, `mota`) VALUES
(1, 'ADIDAS', N'GIÀY ADIDAS ALPHABOUNCE INSTINCT', 3690000, 'G1.jpg', N'Giày thể thao cao cấp Adidas.'),
(2, 'ADIDAS', N'GIÀY ADIDAS Stansmith ', 2200000, 'G2.jpg', N'Giày thể thao cao cấp Adidas'),
(3, 'ADIDAS', N'GIÀY ADIDAS TOP TEN MID PC', 2700000, 'G3.jpg', N'Giày thể thao cao cấp Adidas'),
(4, 'ADIDAS', N'GIÀY ADIZERO BOSTON BOOST 6', 5000000, 'G4.jpg', N'Giày thể thao cao cấp Adidas'),
(5, 'ADIDAS', N'GIÀY ADIDAS DURAMO 8 ', 5300000, 'G5.jpg', N'Giày thể thao cao cấp Adidas'),
(6, 'NIKE', N'GIÀY NIKE METCON 4 PREMIUM XANH CAMO', 6500000, 'G6.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(7, 'NIKE', N'GIÀY NIKE AIR MAX 94', 3500000, 'G7.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(8, 'NIKE', N'GIÀY NIKE AIR MAX 94', 7200000, 'G8.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(9, 'NIKE', N'GIÀY NIKE AIR MAX 94', 3600000, 'G9.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(10, 'NIKE', N'GIÀY NIKE DUALTONE RACER SE', 5500000, 'G10.jpg', N'Giày thể thao cao cấp thương hiệu Nike.'),
(11, 'BALENCIAGA', N'GIÀY balenciaga track 3.0 yellow', 18000000, 'G11.jpg', N'Giày cao cấp Balencia.'),
(12, 'BALENCIAGA', N'GIÀY Balenciaga Track 3.0', 21000000, 'G12.jpg', N'Giày cao cấp Balencia.'),
(13, 'BALENCIAGA', N'GiÀY Balenciaga Tripper S Red Black', 24000000, 'G13.jpg', N'Giày cao cấp Balencia.'),
(14, 'BALENCIAGA', N'GiÀY Balenciaga Tripper S Red White', 24500000, 'G14.jpg', N'Giày cao cấp Balencia.'),
(15, 'BALENCIAGA', N'GiÀY Balenciaga Speed Trainer Balck White', 21500000, 'G15.jpg', N'Giày cao cấp Balencia.'),
(16, 'JeanKimaru', N'Jean xanh Kimaru D20', 380000, 'jean1.jpg', N'Quần jean cao cấp màu xanh kimaru.'),
(17, 'JeanKimaru', N'Jean Nam Kimaru D218', 360000, 'jean2.jpg', N'Quần jean cao cấp màu đen kimaru.'),
(18,'JeanKimaru', N'Jean xanh Nam Kimaru D19', 380000, 'jean3.jpg', N'Quần jean cao cấp màu xanh kimaru.'),
(19, 'JeanKimaru', N'Jean xanh bạc  Kimaru D22', 380000, 'jean4.jpg', N'Quần jean cao cấp màu xanh nhạt kimaru.'),
(20, 'Jean Nostyle', N'Jean Nam Nostyle D45', 400000, 'jean5.jpg', N'Quần jean cao cấp Nostyle D45.'),
(21, 'Jean Nostyle', N'Jean Nam Nostyle A23', 400000, 'jean6.jpg', N'Quần jean cao cấp Nostyle A43.'),
(22, 'Jean Nostyle', N'Jean Nam Nostyle A25', 400000, 'jean7.jpg', N'Quần jean cao cấp Nostyle A25.'),
(23, 'Jean Nostyle', N'Jean Nam Nostyle A39', 400000, 'jean8.jpg', N'Quần jean cao cấp Nostule A39.'),
(24, 'Jean QK2HD', N'Jean Nam QK2HD', 999999, 'jean9.jpg', N'Quần jean phiên bản đặc biệt của hãng J2W.'),
(25, 'Jean QK2HD', N'Jean Nam KSH2', 500000, 'jean10.jpg', N'Sản phẩm đầu hè của J2W.'),
(26, 'Ao Nostyle', N'Thun Nam Nostyle cổ trụ N01', 300000, 'T1.jpg', N'Áo thun cổ trụ cao cấp Nostyle N01.'),
(27, 'Ao Nostyle', N'Thun Nam Nostyle Essential ', 300000, 'T2.jpg', N'Áo thun nostyle chất liệu vải thoáng mát.'),
(28, 'Ao Nostyle', N'Thun Nam Nostyle Colaboss', 300000, 'T3.jpg', N'Thun Nam Nostyle Colaboss siêu mỏng.'),
(29, 'Ao Nostyle', N'Áo thun dài tay N1', 300000, 'T4.jpg', N'Áo dài tay thun mỏng cho mùa hè.'),
(30, 'Ao Basic', N'Áo thun cổ trụ M01', 270000, 'T5.jpg', N'Mát mẽ, sang trọng cho ngày hè.'),
(31, 'Ao Basic', N'Áo thun basic đen B01', 270000, 'T6.jpg', N'Chất liệu vải thấm hút tốt, thiết kế đơn giản nam tính.'),
(32, 'Ao Basic', N'Áo thun basic B02', 270000, 'T7.jpg', N'Thiết kế đơn giản, chất liệu thoáng mát cho ngày hè.'),
(33,'Ao L3', N'Áo thun tay ngắn Ak3', 255000, 'T8.jpg', N'Thiết kế đẹp mắt, năng động.'),
(34, 'Ao L3', N'Áo thun nam bassic N05', 255000, 'T9.jpg', N'Đơn giản. mạnh mẽ.'),
(35, 'Ao L3', N'Áo thun cổ trụ S04', 255000, 'T10.jpg', N'Áo thun cổ trụ hiện đại, năng động.');


CREATE TABLE qlbh.`user` (
  email VARCHAR(45) NOT NULL,
  password VARCHAR(160) NOT NULL,
  name NVARCHAR(45) NULL,
  address NVARCHAR(150) NULL,
  phonenumber INT NULL,
  PRIMARY KEY (email));

CREATE TABLE qlbh.`posts` (
  id INT NOT NULL AUTO_INCREMENT,
  title NVARCHAR(150) NOT NULL,
  content NVARCHAR(6555) NULL,
  author NVARCHAR(45) NULL,
  created_at DATETIME NULL,
  updated_at DATETIME NULL,
  PRIMARY KEY (id));
  
  select * from user

  CREATE TABLE qlbh.`CART` (
  `ID` varchar(50) ,
  `TENSP` varchar(255) ,
  `IMAGE` varchar(50) ,
  `GIA` float,
  `SOLUONG` int
) ;

after table qlbh.`CART` add primary key(ID,TENSP)


CREATE TABLE binhluan (
  stt int  auto_increment,
  tenclient varchar(30),
  hinh varchar(50) ,
  binhluan varchar(1000) ,
  primary key(stt)
) ;
ALTER TABLE binhluan ADD FOREIGN KEY(hinh) REFERENCES product(hinh);
insert into binhluan(tenclient,hinh,binhluan) values
('Huy','G1.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G2.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G3.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G4.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G5.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G6.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G7.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G8.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G9.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G10.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G11.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G12.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G13.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G14.jpg','Chất lượng sản phẩm tốt!'),
('Huy','G15.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean1.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean2.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean3.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean4.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean5.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean6.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean7.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean8.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean9.jpg','Chất lượng sản phẩm tốt!'),
('Huy','jean10.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T1.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T2.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T3.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T4.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T5.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T6.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T7.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T8.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T9.jpg','Chất lượng sản phẩm tốt!'),
('Huy','T10.jpg','Chất lượng sản phẩm tốt!');