var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {


 
res.render('cart');
  

});

module.exports=function Cart(initItems){
    this.item=initItems;
    this.totalQty=0;
    this.totalPrice=0;

    this.add = function(item, id) {
        var storedItem=this.items[id];
        if(!storedItem){
            storedItem=this.item[id]={item: item, qty:0, price:0};
        }
        storedItem.qty++;
        storedItem.price=storedItem.item.price*storedItem.qty;
        this.totalQty++;
        t;his.totalPrice+=storedItem.price;
    };

    this.generateArray=function(){
        var arr=[];
        for(var id in this.item){
            arr.push(this.item[id]);
        }
        return arr;
    };
};

router.get('/add-to-cart/:id',function(req,res,next){
    var productId=req.params.id;
    var cart=new Cart(req.session.cart ? req.session.cart: {});

    productId.findById(productId,function(err,product){
        if(err){
            return res.redirect('/');
        }

        cart.add(product,product.id);
        req.session.cart= cart;
        console.log(req.session.cart);
        res.redirext('/');
    });
});

module.exports = router;
