function Post(){

    function bindEvent(){
        $(".product_edit").click(function(e){
            var params = {
                masp: $(".id").val(),
                maloai: $(".maloai").val(),
                tensp: $(".tensp").val(),
                dongia: $(".dongia").val(),
                hinh: $(".hinh").val(),
                mota: $(".mota").val(),
            };
            var base_url = location.protocol + "//" +document.domain + ":" + location.port;
        //dùng ajax để gọi phương thức cập nhật
            $.ajax({
            url: base_url + "/admin/product/edit",
            type : "PUT",
            data: params,
            dataType : "json",
            success: function(res){
                if(res && res.status_code == 200){
                    location.reload();
                    location.href = base_url + "/admin/product";
                }
            }
        });
    });

    //bắt xự kiện khi click delete
        $(".product_delete").click(function(e){
            //thuộc tính post id
            var product_masp = $(this).attr("product_id");
            var base_url = location.protocol + "//" +document.domain + ":" + location.port;
            //dùng ajax để gọi phương thức xóa
            $.ajax({
                url: base_url + "/admin/product/delete",
                type : "DELETE",
                data: {product_masp : product_masp},
                dataType : "json",
                success: function(res){
                    if(res && res.status_code == 200){
                        //load lại trang để xem kết quả
                        location.reload();
                    }
                }
            });
        });
    }
    bindEvent();
}

$(document).ready(function(){
    new Post(); 
});