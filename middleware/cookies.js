var mysql = require('mysql');
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "qlbh"
  });
module.exports.requireAuth = function(req,res, next){
    var userEmail = req.cookies.admin;
    //nếu không có cookie
    if(!userEmail){
        res.redirect('/require');
        return;
    }
    next();
};