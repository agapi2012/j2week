var q = require("q");
var db = require("../Du_Lieu/connect");
var conn = db.getConnection();
function getAllProduct(){
    var defer = q.defer();
    var query = conn.query('SELECT * FROM product', function(err,data){
        if(err)
        {
            defer.reject(err);
        }
        else{
            defer.resolve(data);
        }
    });
    return defer.promise;
}
function getProductById(masp){
    var defer = q.defer();
        /*id = id  */
        var query = conn.query('SELECT * FROM product WHERE masp = ?',[masp], function(err,result){
        console.log('query------------');
        console.log(query.sql);
            if(err)
            {
                console.log("Query bị lỗi----------------------------------------------------");
                defer.reject(err);
            }else{
                defer.resolve(result);
            }
        });
        return defer.promise;
}

function updateProduct(params){
    if(params){

    var defer = q.defer();
        /*id = id  */
        var query = conn.query('UPDATE product SET maloai = ?, tensp = ?, dongia = ?, hinh = ?, mota = ? WHERE masp = ?',[params.maloai, params.tensp, params.dongia,params.hinh , params.mota, params.masp], function(err,result){
            console.log('query update product -------------');
            console.log(query.sql);

            if(err)
            {
                defer.reject(err);
            }else{
                defer.resolve(result);
            }
        });
        return defer.promise;
    }
        return false;
}

function deleteProduct(masp){
    if(masp){
    var defer = q.defer();
        /*id = id  */
    var query = conn.query('DELETE FROM product WHERE masp = ?',[masp], function(err,result){
            if(err)
            {
                defer.reject(err);
            }else{
                defer.resolve(result);
            }
        });
        console.log('query ok--------------------------------------------');
        return defer.promise;
    }
        console.log('query not ok---------------------------------------------');
        return false;
}
function addProduct(params){
    if(params.masp!=null)
    {
        var defer = q.defer();
        var query = conn.query('INSERT INTO product SET ?',params, function(error, results) {
        console.log('insert query' + query);
        if (error) {
            defer.reject(error);
        }else{
            defer.resolve(results);
        }
    });
    return defer.promise;
    }
    return false;
}

module.exports = {
    getAllProduct : getAllProduct,
    getProductById : getProductById,
    updateProduct : updateProduct,
    deleteProduct: deleteProduct,
    addProduct : addProduct,
};
