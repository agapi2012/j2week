
var q = require("q");
var db = require("../Du_Lieu/connect");
var conn = db.getConnection();
var bcrypt = require('bcrypt');
const saltRounds = 10;
    function addUser(user){
    if(user)
    {
        var defer = q.defer();
        var query = conn.query('INSERT INTO user SET ?', user, function(error, results, fields) {
        if (error) {
            defer.reject(error);
        }else{
            defer.resolve(results);
        }
    });
    return defer.promise;
    }
    return false;
}

function getUserByEmail(email){
    if(email)
    {
        var defer = q.defer();
        var query = conn.query('SELECT * FROM user WHERE ?',{ email: email}, function(err,result){
            if(err)
            {
                defer.reject(err);
            }
            else{
                defer.resolve(result);
            }
        });
        return defer.promise;
    }
    return false;
}


function getAllUser(){
        var defer = q.defer();
        var query = conn.query('SELECT * FROM user', function(err,users){
            if(err)
            {
                defer.reject(err);
            }
            else{
                defer.resolve(users);
            }
        });
        return defer.promise;
}
function editUser(user){
    if(user){

        var defer = q.defer();
            /*id = id  */
            var query = conn.query('UPDATE user SET password = ?, name = ?, address = ?,phonenumber = ? WHERE email = ?',[user.password,user.name, user.address ,user.phonenumber, user.email], function(err,result){
                console.log('query update user -------------');
                console.log(query.sql);
                if(err)
                {
                    defer.reject(err);
                }else{
                    defer.resolve(result);
                }
            });
            return defer.promise;
        }
            return false;
}
function hash_password(password){
    var hash = bcrypt.hashSync(password, saltRounds);
    return hash;
}
function compare_password(password,hash){
    return bcrypt.compareSync(password, hash);
}

module.exports = {
    addUser : addUser,
    getUserByEmail : getUserByEmail,
    getAllUser : getAllUser,
    hash_password : hash_password,
    compare_password : compare_password,
    editUser : editUser
};
