var q = require("q");
var db = require("../Du_Lieu/connect");
var conn = db.getConnection();


function addPost(params){
    if(params)
    {
        var defer = q.defer();
        var query = conn.query('INSERT INTO posts SET ?', params, function(error, results) {
        if (error) {
            defer.reject(error);
        }else{
            defer.resolve(results);
        }
    });
    return defer.promise;
    }
    return false;
}
    function getAllPosts(){
        var defer = q.defer();
        var query = conn.query('SELECT * from posts', function(err, results) {
        if (err) {
            defer.reject(err);
        }else{
            defer.resolve(results);
        }
    });
    return defer.promise;

}

function getPostsById(id){
    var defer = q.defer();
        /*id = id  */
        var query = conn.query('SELECT * FROM posts WHERE id = ?',[id], function(err,result){
        console.log('query------------' + query);
            if(err)
            {
                defer.reject(err);
            }else{
                defer.resolve(result);
            }
        });
        return defer.promise;
}

function updatePost(params){
    if(params){

    var defer = q.defer();
        /*id = id  */
        var query = conn.query('UPDATE posts SET title = ?, hinh = ?, mota = ?,content = ?, author = ?, updated_at = ? WHERE id = ?',[params.title,params.hinh, params.mota ,params.content, params.author, new Date(), params.id], function(err,result){
            console.log('query update product -------------');
            console.log(query.sql);
            if(err)
            {
                defer.reject(err);
            }else{
                defer.resolve(result);
            }
        });
        return defer.promise;
    }
        return false;
}

function deletePost(id){
    if(id){
    var defer = q.defer();
        /*id = id  */
    var query = conn.query('DELETE FROM posts WHERE id = ?',[id], function(err,result){
            if(err)
            {
                defer.reject(err);
            }else{
                defer.resolve(result);
            }
        });
        return defer.promise;
    }
        return false;
}

module.exports = {
    getAllPosts : getAllPosts,
    addPost : addPost,
    getPostsById : getPostsById,
    updatePost : updatePost,
    deletePost : deletePost
};
