var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session=require('express-session')

var config = require('config');
var bcrypt = require('bcrypt');
const saltRounds = 10;

var cookie_md = require("./middleware/cookies");
var indexRouter = require('./routes/index');
var nikeRouter = require('./routes/nike');
var adidasRouter= require('./routes/adidas');
var balenciaRouter= require('./routes/balencia');
var hyper_Router= require('./routes/hyper_');
var otherRouter= require('./routes/other');
var jeansRouter= require('./routes/jeans');
var tshirtRouter= require('./routes/tshirt');
var shopRouter= require('./routes/shop');
var aboutRouter= require('./routes/about');
var contactRouter= require('./routes/contact');
var blogRouter= require('./routes/blog');
//var productRouter= require('./routes/product');
var xulyRouter= require('./routes/xuly');
var errorRouter= require('./routes/404');
var sentmailRouter= require('./routes/sentmail');
var cartRouter= require('./routes/cart');
var orderRouter= require('./routes/order');
var xulydanhmucRouter= require('./routes/xulydanhmuc');
var sentDetailsBill = require('./routes/sent_details_bill');

var editUser= require('./routes/editUser');
var requireLogin= require('./routes/requireLogin');

var loginRouter= require('./routes/login');
var signupRouter= require('./routes/signup');
var adminRouter= require('./routes/admin');
// var newpostRouter= require('./routes/newpost');


var app = express();
var mysql = require('mysql');
var csdl = require('./Du_Lieu/csdl');



app.use(session({ secret: 'keyboard cat', resave: true, saveUninitialized: true }))
// app.use((req,res,next)=>{
//   // req.session = {
//   //   user:'quoc', tensp:[], gia:[],soluong:[]
//   // }
//   if(req.session.count!=1){
//     req.session.user='guest';
//     req.session.image=[];
//     req.session.tensp=[];
//     req.session.soluong=[];
//     req.session.gia=[];
//     req.session.count=1;
//     req.session.total=0;
//     next();
//   }
//   else{
//     if(req.session.user!="guest"){
//       con.connect(function (err){
//         var sql="select * from cart where ID like'%"+req.session.user+"%'"
//         result = con.query(sql, function (err,results, fields) {
//           if(results.length!=0){
//             for(i=0;i<results.length;i++){
//               req.session.total+=parseInt(results[i].SOLUONG)
//             }
//           }
//         })
//       })
//     }
//     next();
//   }

//   })
//Kết nối sql
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "qlbh"
});
app.use((req,res,next)=>{
  // req.session = {
  //   user:'quoc', tensp:[], gia:[],soluong:[]
  // }
  if(req.session.count!=1){
    req.session.user='guest';
    req.session.image=[];
    req.session.tensp=[];
    req.session.soluong=[];
    req.session.gia=[];
    req.session.count=1;
    req.session.total=0;
    next();
  }
  else{
    if(req.session.user!='guest'){
      //console.log("DA VAO-------------------------------------------------------")
      con.connect(function (err){
        var sql="select * from cart where ID like'%"+req.session.user+"%'"
        result = con.query(sql, function (err,results, fields) {
          // console.log(results.length)
          req.session.total=0;
          if(results.length!=0){
            for(i=0;i<results.length;i++){
              // console.log(results[i].SOLUONG)
              req.session.total+=parseInt(results[i].SOLUONG)
            }
          }
          // console.log(req.session.total)
        })
      })
    }
    next();
  }

  })
// if(req.session.user!="guest"){
//   con.connect(function (err){
//     var sql="select * from cart where ID like'%"+req.session.user+"%'"
//     result = con.query(sql, function (err,results, fields) {
//       if(results.length!=0){
//         for(i=0;i<results.length;i++){
//           req.session.total+=parseInt(results[i].SOLUONG)
//         }
//       }
//     })
//   })
// }



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/nike', nikeRouter);
app.use('/adidas', adidasRouter);
app.use('/balencia', balenciaRouter);
app.use('/hyper_', hyper_Router);
app.use('/other', otherRouter);
app.use('/jeans', jeansRouter);
app.use('/tshirt', tshirtRouter);
app.use('/shop', shopRouter);
app.use('/about', aboutRouter);
app.use('/contact', contactRouter);
app.use('/blog', blogRouter);
app.use('/xuly', xulyRouter);
app.use('/sentmail', sentmailRouter);
app.use('/login', loginRouter);
app.use('/signup', signupRouter);
app.use('/admin',cookie_md.requireAuth, adminRouter);
app.use('/cart', cartRouter);
app.use('/order',orderRouter);
app.use('/xulydanhmuc', xulydanhmucRouter);
app.use('/cart/sent-details-bill', sentDetailsBill);
app.use('/editUser', editUser);
app.use('/require', requireLogin);
// app.use('/admin/post/news', newpostRouter);
app.use('/*', errorRouter);


// Chạy từng dòng, chú ý thứ tự
//app.use('/*', productRouter);


var user_md = require("./models/user");
var post_md = require("./models/post");


var session = require('express-session');
app.set('trust proxy', 1); //s trust first proxy
app.use(session({
  secret: 'key',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}));


const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));


app.post("/login", function(req,res){
  var info = req.body;
  console.log(info);
  if(info.email == ''){
    res.render("login", {data : {error : "Enter an email"}});
  }
  console.log(info.email);
  con.connect(function(err) {

    con.query("SELECT * FROM user WHERE email = '"+ info.email +"'", function(err, results, fields) {
      var data = results;
        if(data.length != 0){
          //mảng user lấy từ db
          user = data[0];
          if(!bcrypt.compareSync(info.password, user.password)){
            res.render("login" , {data : {error : "Password's wrong"}}); 

          }else{
            req.session.user = user.email;
            console.log('email---------------------------------------');
            console.log(user.email);
            console.log(req.session.user);
            res.redirect('/');
            }
        }else{
      res.render("login" , {data : {error : "Email isn't existed"}});
    }
  });
  });
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;



