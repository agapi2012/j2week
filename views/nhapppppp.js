<!DOCTYPE html>
<html lang="en">

<head>
	<title>J2Week</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.css">

	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">

	<link rel="stylesheet" href="css/aos.css">

	<link rel="stylesheet" href="css/ionicons.min.css">

	<link rel="stylesheet" href="css/bootstrap-datepicker.css">
	<link rel="stylesheet" href="css/jquery.timepicker.css">

	<!--css for button-->
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/icomoon.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<script language="javascript" type="text/javascript">
	function Timkiem() {
		window.location = "http://localhost:3000/?key=" + document.getElementById("ten").value
	}

</script>
<script language="javascript" type="text/javascript">
	function LocGia() {
		window.location = "http://localhost:3000/?Pfrom=" + document.getElementById("people").value + "&Pto=" + document.getElementById("people1").value
	}
</script>

<body class="goto-here">
	<!-- thanh menu và navigation-->
	<div class="py-1 bg-black">
		<div class="container">
			<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
				<div class="col-lg-12 d-block">
					<div class="row d-flex">
						<div class="col-md pr-4 d-flex topper align-items-center">
							<div class="icon mr-2 d-flex justify-content-center align-items-center"><span
									class="icon-phone2"></span></div>
							<span class="text">+ 1235 2355 98</span>
						</div>
						<div class="col-md pr-4 d-flex topper align-items-center">
							<div class="icon mr-2 d-flex justify-content-center align-items-center"><span
									class="icon-paper-plane"></span></div>
							<span class="text">hangdtt@gmail.com</span>
						</div>
						<div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
							<span class="text">3-5 Business days delivery &amp; Free Returns</span>
						</div>

						<div class="col-md pr-4 d-flex topper align-items-center text-lg-right">
							<span class="text"><a href="./login">Signin / Signup</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
		<div class="container">
			<a class="navbar-brand" href="./" style="font-size:30px;">J2WEEK</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
				aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="oi oi-menu"></span> Menu
			</button>

			<div class="collapse navbar-collapse" id="ftco-nav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item"><a href="./" class="nav-link" style="font-size: 12px;"><b>Home</b></a></li>
					<li class="nav-item"><a href="./shop" class="nav-link" style="font-size: 12px;"><b>Shop</b></a></li>
					<li class="nav-item"><a href="./about" class="nav-link" style="font-size: 12px;"><b>About</b></a>
					</li>
					<li class="nav-item"><a href="./blog" class="nav-link" style="font-size: 12px;"><b>Blog</b></a></li>
					<li class="nav-item"><a href="./contact" class="nav-link"
							style="font-size: 12px;"><b>Contact</b></a></li>
					<li class="nav-item"><a href="./admin" class="nav-link" style="font-size: 12px;"><b>Admin</b></a>
					</li>
					<li class="nav-item cta cta-colored"><a href="cart.html" class="nav-link"
							style="font-size: 12px;"><span class="icon-shopping_cart"></span>[0]</a></li>
					<!--Thanh tìm kiếm-->
					<li class="nav-item"><span class="nav-link">
							<input align="right" name="ten" type="text" id="ten" size="35">
							<button name="search" style="background-color:#000; border-color:#000" onClick="Timkiem();">
								<img width="15" height="15" src="images/Layout/search1.png" />
							</button>
						</span>
					</li>
					<!--<a href= "#" ><img width="20" height="20" src="images/Search.png"/></a>-->
				</ul>
			</div>
		</div>
	</nav>
	<!-- END nav -->
	<!-- END menu và navigation -->

	<!-- Hình ảnh đầu trang-->
	<div class="hero-wrap hero-bread" style="background-image: url('images/Layout/bg_6.jpg');">
		<div class="container">
			<div class="row no-gutters slider-text align-items-center justify-content-center">
				<div class="col-md-9 ftco-animate text-center">
					<p class="breadcrumbs"><span class="mr-2"><a href="./">Home</a></span> <span>Shop</span></p>
					<h1 class="mb-0 bread">Shop</h1>
				</div>
			</div>
		</div>
	</div>




	<!--Thân trang-->
	<!--Hiển thị sản phẩm-->
	<section class="ftco-section bg-light">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-10 order-md-last">
					<!-- <div class="row loadphantrang" >
					</div> -->
					<div class="row loadphantrang">
							
							</div>
					
						<!--End hiển thị sản phẩm-->

						<!--Thanh qua trang-->
						<div class="row mt-5">
							<div class="col text-center">
								<div class="block-27">
									
									<ul id="phantrang" ></ul>
                          			 <input type="hidden" id="giatriphantrang">
							
							</div>
							</div>
						</div>
					</div>
					<!-- End thanh qua trang-->
					<!-- Bảng chọn Categories-->
					<div class="col-md-4 col-lg-2">
						<div class="sidebar">
							<div class="sidebar-box-2">
								<h2 class="heading">Categories</h2>
								<div class="fancy-collapse-panel">
									<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingOne">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion"
														href="#collapseOne" aria-expanded="true"
														aria-controls="collapseOne">Shoes
													</a>
												</h4>
											</div>
											<div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="headingOne">
												<div class="panel-body">
													<ul id="phantrangsp">
														<li><a href="./adidas">Adidas</a></li>
														<li><a href="./nike">Nike</a></li>
														<li><a href="./balencia">Balenciaga</a></li>
													</ul>
												</div>
											</div>
										</div>

										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="headingThree">
												<h4 class="panel-title">
													<a class="collapsed" data-toggle="collapse" data-parent="#accordion"
														href="#collapseThree" aria-expanded="false"
														aria-controls="collapseThree">Clothing
													</a>
												</h4>
											</div>
											<div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
												aria-labelledby="headingThree">
												<div class="panel-body">
													<ul id="phantrangsp1">
														<li><a href="./jeans">Jean</a></li>
														<li><a href="./tshirt">Ao</a></li>

													</ul>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
							<!--End bảng chọn categories-->
							<!-- Hộp thoại lọc theo giá-->
							<div class="sidebar-box-2">
								<h2 class="heading">Price Range</h2>
								<!--	<form method="post" class="colorlib-form-2" >  -->
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="guests">Price from:</label>
											<div class="form-field">
												<i class="icon icon-arrow-down3"></i>
												<select name="people" id="people" class="form-control">
													<option value="500000">500000</option>
													<option value="1000000">1000000</option>
													<option value="5000000">5000000</option>
													<option value="10000000">10000000</option>

												</select>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label for="guests">Price to:</label>
											<div class="form-field">
												<i class="icon icon-arrow-down3"></i>
												<select name="people1" id="people1" class="form-control">
													<option value="2000000">2000000</option>
													<option value="4000000">4000000</option>
													<option value="10000000">10000000</option>
													<option value="20000000">20000000</option>
												</select>
											</div>

										</div>
									</div>
								</div>
								</form>
								<button style="background-color:#000; border-color:#000" name="loc"
									onClick="LocGia();"><Span style="color:#FFF">Filter</Span></button>
							</div>
							<!--End hộp thoại lọc theo giá-->
						</div>
					</div>
				</div>
			</div>
	</section>
	<!--End thân trang-->
	<!--Phần mô tả trên footer-->
	<section class="ftco-gallery">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 heading-section text-center mb-4 ftco-animate">
					<h2 class="mb-4">Follow Us On Instagram</h2>
					<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there
						live the blind texts. Separated they live in</p>
				</div>
			</div>
		</div>
		<div class="container-fluid px-0">
			<div class="row no-gutters">
				<div class="col-md-4 col-lg-2 ftco-animate">
					<a href="images/Layout/gallery-1.jpg" class="gallery image-popup img d-flex align-items-center"
						style="background-image: url(images/Layout/gallery-1.jpg);">
						<div class="icon mb-4 d-flex align-items-center justify-content-center">
							<span class="icon-instagram"></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-lg-2 ftco-animate">
					<a href="images/Layout/gallery-2.jpg" class="gallery image-popup img d-flex align-items-center"
						style="background-image: url(images/Layout/gallery-2.jpg);">
						<div class="icon mb-4 d-flex align-items-center justify-content-center">
							<span class="icon-instagram"></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-lg-2 ftco-animate">
					<a href="images/Layout/gallery-3.jpg" class="gallery image-popup img d-flex align-items-center"
						style="background-image: url(images/Layout/gallery-3.jpg);">
						<div class="icon mb-4 d-flex align-items-center justify-content-center">
							<span class="icon-instagram"></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-lg-2 ftco-animate">
					<a href="images/Layout/gallery-4.jpg" class="gallery image-popup img d-flex align-items-center"
						style="background-image: url(images/Layout/gallery-4.jpg);">
						<div class="icon mb-4 d-flex align-items-center justify-content-center">
							<span class="icon-instagram"></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-lg-2 ftco-animate">
					<a href="images/Layout/gallery-5.jpg" class="gallery image-popup img d-flex align-items-center"
						style="background-image: url(images/Layout/gallery-5.jpg);">
						<div class="icon mb-4 d-flex align-items-center justify-content-center">
							<span class="icon-instagram"></span>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-lg-2 ftco-animate">
					<a href="images/Layout/gallery-6.jpg" class="gallery image-popup img d-flex align-items-center"
						style="background-image: url(images/Layout/gallery-6.jpg);">
						<div class="icon mb-4 d-flex align-items-center justify-content-center">
							<span class="icon-instagram"></span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>
	<!--End mô tả trên footer-->
	<!-- Footer-->
	<footer class="ftco-footer ftco-section">
		<div class="container">
			<div class="row">
				<div class="mouse">
					<a href="#" class="mouse-icon">
						<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
					</a>
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-md">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2" style="font-size: 25px; font-weight: bold;">J2WeeK</h2>
						<p>We always want to bring our customers the best experience.</p> 
						<p>Welcome to you come here!</p>
						<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
							<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
							<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
							<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md">
					<div class="ftco-footer-widget mb-4 ml-md-5">
						<h2 class="ftco-heading-2">Menu</h2>
						<ul class="list-unstyled">
							<li><a href="./shop" class="py-2 d-block">Shop</a></li>
							<li><a href="./about" class="py-2 d-block">About</a></li>
							<li><a href="./contact" class="py-2 d-block">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Help</h2>
						<div class="d-flex">
							<ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
								<li><a href="#" class="py-2 d-block">Shipping Information</a></li>
								<li><a href="#" class="py-2 d-block">Returns &amp; Exchange</a></li>
								<li><a href="#" class="py-2 d-block">Terms &amp; Conditions</a></li>
								<li><a href="#" class="py-2 d-block">Privacy Policy</a></li>
							</ul>
							<ul class="list-unstyled">
								<li><a href="#" class="py-2 d-block">FAQs</a></li>
								<li><a href="#" class="py-2 d-block">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md">
					<div class="ftco-footer-widget mb-4">
						<h2 class="ftco-heading-2">Have a Questions?</h2>
						<div class="block-23 mb-3">
							<ul>
								<li><span class="icon icon-map-marker"></span><span class="text">203 Fake St. Mountain
										View, San Francisco, California, USA</span></li>
								<li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929
											210</span></a></li>
								<li><a href="#"><span class="icon icon-envelope"></span><span
											class="text">quachphuquoc@yourdomain.com</span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<p>
						Copyright &copy;
						<script>document.write(new Date().getFullYear());</script> All rights reserved
					</p>
				</div>
			</div>
		</div>
	</footer>
	<!--End Footer-->


	<!-- <script>
 
			// function removeCart(event){
			// 	alert(event.target.parentElement)
			// 	event.target.parentElement.remove()
			// }
			let length = document.getElementsByClassName('add-to-cart text-center py-2 mr-1').length;
			let length2=document.getElementsByClassName('img-fluid').length;
			let length3=document.getElementsByClassName('price').length;
			for(let i = 0; i<length; i++){
				document.getElementsByClassName('add-to-cart text-center py-2 mr-1')[i].addEventListener('click', function(){
					// document.getElementsByClassName('add-to-cart text-center py-2 mr-1')[i].parentElement.remove();
					
					let tensp = document.getElementsByClassName('add-to-cart text-center py-2 mr-1')[i].parentElement.parentElement.childNodes[1].textContent;
					let image= document.getElementsByClassName('img-fluid')[i].alt
					let price=document.getElementsByClassName('price')[i].childNodes[0].textContent;
          			price=price.slice(0,price.indexOf("VND"));
					var xhr = new XMLHttpRequest();
						xhr.open("GET", `http://localhost:3000/shop/${tensp}/${image}/${price}`, true);
						xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
						
						xhr.send();
						alert("Add"+tensp+"/n"+image+"/n"+price);
				})
			}
		
		  </script> -->
	<!-- loader -->
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
			<circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
			<circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
				stroke="#F96D00" /></svg></div>


	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-migrate-3.0.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.stellar.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/aos.js"></script>
	<script src="js/jquery.animateNumber.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/scrollax.min.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
	<script src="js/google-map.js"></script>
	<script src="js/main.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.1/jquery.twbsPagination.min.js"></script>
	<script>
			$(document).ready(function(){
				phantrang(1,"http://localhost:3000/xuly");
				function phantrang(page,url){
					$.ajax({
					url:url,
					data:{page:page},
					method:"get",
					dataType:"Json",
					success:function(data){
						var kq='';
						for (var i =0; i < data.products.length; i++){
						   kq+="<div class='col-sm-12 col-md-12 col-lg-4'>"
						   kq+="<div class='product'>"
						   kq+="<a href='./?ma="+ data.products[i].hinh +"' class='img-prod'><img class='img-fluid' src='images/Total/" + data.products[i].hinh + "' alt='"+data.products[i].hinh+"'>"
						   kq+="<div class='overlay'></div> </a>"
						   kq+="<div class='text py-3 pb-4 px-3'> <div class='d-flex'> <div class='cat'>"
						   kq+="<span>Lifestyle</span></div></div>"
						   kq+="<h3><a href='#'>" + data.products[i].tensp +"</a></h3><div class='pricing'> <p class='price'><span>"+ data.products[i].dongia +"VND</span></p></div>"
						   kq+="<p class='bottom-area d-flex px-3'>"
						   kq+="<a href='#' class='add-to-cart text-center py-2 mr-1'><span>Add to cart <i class='ion-ios-add ml-1'></i></span></a>"
						   kq+="<a href='#' class='buy-now text-center py-2'>Buy now<span><i class='ion-ios-cart ml-1'></i></span></a></p></div></div></div></div>"
						}
						$(".loadphantrang").html(kq);
						document.getElementsByClassName('nav-item cta cta-colored')[0].childNodes[0].textContent = `[${data.totalCart}]`
						ADD_TO_CART();
						console.log(kq);
					}
					});
				};
				//phan trang
				
			// $("#phantrang").on("click","li",function(e){
			//    e.preventDefault();
			   
			//    var a= $(this).parent().find(".active");
			//    $(a).removeClass("active");
			//    //console.log(a)
			//    $(this).addClass("active");
			
			// 	var page = $(this).text();
			// 	phantrang(page);
			// 	return $("body, html").animate({scrollTop:500},400),!1

			// })	
				
			});
			//ajax cho danh muc giay
			function phantrangsp(loai,url){
         		$.ajax({
         		url:url,
         		data:{loai:loai},
         		method:"get",
         		dataType:"Json",
         		success:function(data){
					 var kq='';
					 for (var i =0; i <  data.products.length; i++){
						kq+="<div class='col-sm-12 col-md-12 col-lg-4'>"
						kq+="<div class='product'>"
						kq+="<a href='./?ma="+  data.products[i].hinh +"' class='img-prod'><img class='img-fluid' src='images/Total/" +  data.products[i].hinh + "' alt='"+data.products[i].hinh+"'>"
						kq+="<div class='overlay'></div> </a>"
						kq+="<div class='text py-3 pb-4 px-3'> <div class='d-flex'> <div class='cat'>"
						kq+="<span>Lifestyle</span></div></div>"
						kq+="<h3><a href='#'>" +  data.products[i].tensp +"</a></h3><div class='pricing'> <p class='price'><span>"+  data.products[i].dongia +"VND</span></p></div>"
						kq+="<p class='bottom-area d-flex px-3'>"
						kq+="<a href='#' class='add-to-cart text-center py-2 mr-1' dataid='"+ data.products[i].hinh+"'><span>Add to cart <i class='ion-ios-add ml-1'></i></span></a>"
						kq+="<a href='#' class='buy-now text-center py-2'>Buy now<span><i class='ion-ios-cart ml-1'></i></span></a></p></div></div></div></div>"
					 }
					 $(".loadphantrang").html(kq);
					 document.getElementsByClassName('nav-item cta cta-colored')[0].childNodes[0].textContent = `[${data.totalCart}]`
					 ADD_TO_CART();
					 $("#phantrang").hide();
					 console.log(kq);
         		}
         		});
         	};
         ///lenh click
         $("#phantrangsp").on("click","li",function(e){
         e.preventDefault();
		 var loai = $(this).text();
			
		phantrangsp(loai,"http://localhost:3000/xulydanhmuc")
		
		 })
		
		 //ajax cho danh muc quan ao
		
         ///lenh click
         $("#phantrangsp1").on("click","li",function(e){
         e.preventDefault();
		 var loai = $(this).text();
			
			phantrangsp(loai,"http://localhost:3000/xulydanhmuc")
			$("#phantrang").hide();
		 })
		 	 //pagination
		$('#phantrang').twbsPagination({
        totalPages: 4,
        visiblePages: 3,
        next: 'Next',
        prev: 'Prev',
        paginationClass:"",
        anchorClass:"",
        onPageClick: function (event, page) {
         $.ajax({
         		url:"http://localhost:3000/xuly",
         		data:{page:page},
         		method:"get",
         		dataType:"Json",
         		success:function(data){
					 var kq='';
					 for (var i =0; i < data.length; i++){
						kq+="<div class='col-sm-12 col-md-12 col-lg-4'>"
						kq+="<div class='product'>"
						kq+="<a href='./?ma="+ data[i].hinh +"' class='img-prod'><img class='img-fluid' src='images/Giay/" + data[i].hinh + "' alt='Colorlib Template'>"
						kq+="<div class='overlay'></div> </a>"
						kq+="<div class='text py-3 pb-4 px-3'> <div class='d-flex'> <div class='cat'>"
						kq+="<span>Lifestyle</span></div></div>"
						kq+="<h3><a href='#'>" + data[i].tensp +"</a></h3><div class='pricing'> <p class='price'><span>"+ data[i].dongia +"VND</span></p></div>"
						kq+="<p class='bottom-area d-flex px-3'>"
						kq+="<a href='#' class='add-to-cart text-center py-2 mr-1'  dataid='"+data[i].hinh+"'><span>Add to cart <i class='ion-ios-add ml-1'></i></span></a>"
						kq+="<a href='#' class='buy-now text-center py-2'>Buy now<span><i class='ion-ios-cart ml-1'></i></span></a></p></div></div></div></div>"
					 }
                $(".loadphantrang").html(kq);
                return $("body, html").animate({scrollTop:500},400),!1
					// console.log(kq);
         		}
         		});
        }
	});

		 </script>

<script>
 
	// function removeCart(event){
	// 	alert(event.target.parentElement)
	// 	event.target.parentElement.remove()
	// }
	function ADD_TO_CART(){
		// document.getElementsByClassName('nav-item cta cta-colored')[0].childNodes[0].textContent = `[${parseInt(document.getElementsByClassName('nav-item cta cta-colored')[0].childNodes[0].textContent.replace(/[_\W]+/g, "")) + 1}]`
		let length = document.getElementsByClassName('add-to-cart text-center py-2 mr-1').length;
		let length2=document.getElementsByClassName('img-fluid').length;
		let length3=document.getElementsByClassName('price').length;
		for(let i = 0; i<length; i++){
			document.getElementsByClassName('add-to-cart text-center py-2 mr-1')[i].addEventListener('click', function(){
				// document.getElementsByClassName('add-to-cart text-center py-2 mr-1')[i].parentElement.remove();
				
				let tensp = document.getElementsByClassName('add-to-cart text-center py-2 mr-1')[i].parentElement.parentElement.childNodes[2].textContent;
				let image= document.getElementsByClassName('img-fluid')[i].alt
				let price=document.getElementsByClassName('price')[i].childNodes[0].textContent;
				price=price.slice(0,price.indexOf("VND"));
				var xhr = new XMLHttpRequest();
					xhr.open("GET", `http://localhost:3000/shop/${tensp}/${image}/${price}`, true);
					xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
					
					xhr.send();
					alert("Add"+tensp+"/n"+image+"/n"+price);
			})
		}
	}
	

  </script>

</body>

</html>