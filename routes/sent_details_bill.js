var express = require('express');
var router = express.Router();
var nodemailer = require("nodemailer");
var bodyParser = require('body-parser');

router.use(bodyParser.json());

router.use(bodyParser.urlencoded({ extended: false }));

/* GET home page. */
router.post('/', function(req, res, next) {
    var body = req.body;
    var dulieu = JSON.parse(body.data);

    console.log(dulieu);

    //xử lý gửi mail
    var user = "hunghoangsport@gmail.com";
    var pass = "tiendat148";

    var d = new Date();
    var subjectMail = "Xác nhận đơn đặt hàng từ J2Week. Ngày "+d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
        subjectMail += " lúc "+d.getHours()+":"+d.getMinutes();
    var contentMail = "Xin chào "+dulieu.name+"!,\n"+"Chúng tôi gửi mail này để xác nhận rằng quý khách đã đặt mua những món hàng sau:\n";
    for (i = 0; i<dulieu.bill.productName.length; i++){
        contentMail += (i+1)+") "+dulieu.bill.productName[i]+" - đơn giá: "+dulieu.bill.productPrice[i] + 
                    " - số lượng: "+dulieu.bill.quantity[i]+"\n"
    }
    contentMail+="Tổng tiền phải thanh toán: "+dulieu.bill.subtotal+".\n";
    contentMail+="Chân thành cảm ơn quý khách đã ủng hộ cửa hàng của chúng tôi.\n"+"J2WEEK.";

    console.log("=============Chi tiet mail================");
    console.log("subject: "+subjectMail);
    console.log(contentMail);

    const option = {
        host: "smtp.gmail.com",
        port: 587,
        secure: false, // upgrade later with STARTTLS
        auth: {
            user: user,
            pass: pass
        }
    };

    var transporter = nodemailer.createTransport(option);

    transporter.verify(function(error, success){
        if (error) {
            console.log(error);
        } else { //Nếu thành công.
            console.log('Kết nối thành công!');
            var mail = {
                from: user, // Địa chỉ email của người gửi
                to: dulieu.email, // Địa chỉ email của người nhận
                subject: subjectMail, // Tiêu đề mail
                text: contentMail, // Nội dung mail dạng text
            };
            //Tiến hành gửi email
            transporter.sendMail(mail, function(error, info) {
                if (error) { // nếu có lỗi
                    console.log(error);
                } else { //nếu thành công
                    console.log('Email sent: ' + info.response);
                }
            });
        }
    });

    res.send(dulieu);
});

module.exports = router;