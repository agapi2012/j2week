var express = require('express');
var router = express.Router();
var user_md = require("../models/user");
var mysql = require('mysql');
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "qlbh"
  });

var bcrypt = require('bcrypt');
const saltRounds = 10;
/* GET users listing. */
router.get('/', function(req, res, next) {
  var userEmail = req.cookies.user_email;
  var getdata = user_md.getUserByEmail(userEmail);
  if(getdata){
    getdata.then(function(users){
      console.log('db user--------------');

      var user = users[0];
      console.log(user);
      var data = {
        user : user,
        error : false,
      };
      res.render("editUser" , {data:data});
    /*lỗi khi mysql chạy*/ 
    }).catch(function(err){
      var data = {
        error: "My sql err"
      };
      res.render("editUser", {data: data});  
    });
  }
  else{
    var data = {
      error: "Couldn't get Post by id"
    };
    res.render('editUser',{data: data});
  }

});

router.post("/", function(req,res){
  var info = req.body;
               var password = bcrypt.hashSync(info.password, saltRounds);
                //Insert to DB
                user = {
                    email: req.cookies.user_email,
                    password: password,
                    name: info.name,
                    address: info.address,
                    phonenumber: info.phone,
                };
                var result = user_md.editUser(user);
            result.then(function(data){
              res.redirect("/");
          }).catch(function(err){
              var data = {
                error : "Lỗi",
              };
              res.json("signup", {data:{error}});
          });
        });

module.exports = router;