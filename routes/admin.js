let express = require("express");
let router = express();
var post_md = require("../models/post");
var user_md = require("../models/user");
var product_md = require("../models/product");
let bodyParser = require("body-parser");

router.use(bodyParser.urlencoded({extended: true}));
var multer  = require('multer');

// Khởi tạo biến cấu hình cho việc lưu trữ file upload
var diskStorage = multer.diskStorage({
  destination: (req, file, callback) => {
    // Định nghĩa nơi file upload sẽ được lưu lại
    callback(null, "./public/images/Total");
  },
  filename: (req, file, callback) => {
    // ở đây các bạn có thể làm bất kỳ điều gì với cái file nhé.
    // Mình ví dụ chỉ cho phép tải lên các loại ảnh png & jpg
    var math = ["image/png", "image/jpeg"];
    if (math.indexOf(file.mimetype) === -1) {
      var errorMess = `The file <strong>${file.originalname}</strong> is invalid. Only allowed to upload image jpeg or png.`;
      return callback(errorMess, null);
    }
    // Tên của file thì mình nối thêm một cái nhãn thời gian để đảm bảo không bị trùng.
    var d = new Date();
    var filename = `${d.getDate()}-upfile-${file.originalname}`;
    callback(null, filename);
  }
});


var diskStorage2 = multer.diskStorage({
  destination: (req, file, callback) => {
    // Định nghĩa nơi file upload sẽ được lưu lại
    callback(null, "./public/images/Post");
  },
  filename: (req, file, callback) => {
    // ở đây các bạn có thể làm bất kỳ điều gì với cái file nhé.
    // Mình ví dụ chỉ cho phép tải lên các loại ảnh png & jpg
    var math = ["image/png", "image/jpeg"];
    if (math.indexOf(file.mimetype) === -1) {
      var errorMess = `The file <strong>${file.originalname}</strong> is invalid. Only allowed to upload image jpeg or png.`;
      return callback(errorMess, null);
    }
    // Tên của file thì mình nối thêm một cái nhãn thời gian để đảm bảo không bị trùng.
    var d = new Date();
    var filename = `${d.getDate()}-upfile-${file.originalname}`;
    callback(null, filename);
  }
});
// Khởi tạo middleware uploadFile với cấu hình như ở trên,
// Bên trong hàm .single() truyền vào name của thẻ input, ở đây là "file"
var uploadFile = multer({storage: diskStorage}).single("hinh");
var uploadFile2 = multer({storage: diskStorage}).single("uphinh");
var uploadFile3 = multer({storage: diskStorage2}).single("posthinh");
router.get("/", function(req,res){
  console.log('cookie admin--------');
    console.log(req.cookies.admin);

    var getdata = post_md.getAllPosts();
    getdata.then(function(posts){
        var data = {
          posts: posts,
          error : false
        };
        res.render("admin/listPost", {data: data});
    }).catch(function(err){
      res.render("admin/listPost", {data: {err: "Get post data is Error"}});
    });
  });


router.get("/post/news", function(req,res){
    res.render("admin/post/addPost" , {data:{error: false}});
  });

// Router up post mới
router.post("/post/news", function(req,res){
  // Thực hiện upload file, truyền vào 2 biến req và res
  uploadFile3(req, res, (error) => {
    // Nếu có lỗi thì trả về lỗi cho client.
    // Ví dụ như upload một file không phải file ảnh theo như cấu hình của mình bên trên
  if (error) {
    var data = {
      error :`Error when trying to upload: ${error}`
    };
    console.log("lỗi up hình");
    res.render("admin/post/addPost", {data: data});
  }

  console.log(`------Request body-----`);
  console.log(req.body);

  console.log(`------Request file-----`);
  console.log(req.file.filename);

  console.log(`------Test Done-----`);
  var params = {
    title : req.body.title,
    hinh : req.file.filename,
    mota : req.body.mota,
    content : req.body.content,
    author : req.body.author
  }
    //nhận params từ form

    if(params.title.trim().length == 0){
      var data={
        error:"Please enter a title"
      };
      res.render("admin/post/addPost", {data : data});
    }
    else
    {
      var now = new Date();
      params.created_at = now;
      params.updated_at = now;

      //khởi tạo biến data nhận đối tượng promise 
      //đối tượng promise này lấy params do người dùng nhập váo
      var getdata = post_md.addPost(params);
      getdata.then(function(result){
        res.redirect("/admin");
      }).catch(function(err){
        var data = {
          error :"Couldn't insert posts"
        };
        console.log("lỗi rồi add post");
        res.render("admin/post/addPost", {data: data});
      });
    }
  });
  });



router.get("/post/edit/:id", function(req,res){
    //lấy data từ trang web
    var params = req.params;
    var id = params.id;
    //lấy data từ csdl
    var getdata = post_md.getPostsById(id);
    if(getdata){
      getdata.then(function(posts){
        var post = posts[0];
        var data = {
          post : post,
          error : false,
        };
        res.render("admin/post/editPost" , {data:data});
      /*lỗi khi mysql chạy*/ 
      }).catch(function(err){
        var data = {
          error: "My sql err"
        };
        res.render("admin/post/editPost", {data: data});  
      });
    }
    else{
      var data = {
        error: "Couldn't get Post by id"
      };
      res.render("admin/post/editPost", {data: data});  
    }
  });



router.post("/post/edit", function(req,res){
  uploadFile3(req, res, (error) => {
    // Nếu có lỗi thì trả về lỗi cho client.
    // Ví dụ như upload một file không phải file ảnh theo như cấu hình của mình bên trên
    if (error) {
      var data = {
        error :`Error when trying to upload: ${error}`
      };
      console.log("lỗi up hình");
      res.render("admin/post/editPost", {data: data});
    }
    else{
    console.log(`------Request body-----`);
    console.log(req.body);

    console.log(`------Request file-----`);
    console.log(req.file.filename);

    console.log(`------Test Done-----`);
  }
  
  var params = {
    id : req.body.id,
    title : req.body.title,
    hinh : req.file.filename,
    mota : req.body.mota,
    content : req.body.content,
    author : req.body.author
  }
  console.log('param----------');
  
  console.log(params);
  
  getdata = post_md.updatePost(params);
  if(!getdata){
    var data = {
      error : 'Lỗi 2'
    };
    res.render("admin/post/editPost", {data: data});
  }
  else{
    getdata.then(function(result){
      res.redirect('/admin');
    }).catch(function (err) {
      res.send("error");
    });
  }
});
  });
router.delete("/post/delete", function(req,res){
    //lấy id từ web
      var post_id = req.body.id;
      console.log("----------------post_id" + post_id);
      var getdata = post_md.deletePost(post_id);
  
      if(!getdata){
        res.json({status_code: 500});
      }else{
        getdata.then(function(result){
          res.json({status_code: 200});
        }).catch(function (err) {
          res.json({status_code:500});
      });
      }
  });

  router.get("/user", function(req,res){
    var getdata = user_md.getAllUser();

    getdata.then(function(users){
      var data = {
        users : users,
        error : false
      };
      res.render("admin/listUser", {data: data});
    }).catch(function(err){
      var data = {
        error : "Couldn't get user infor"
      };
      res.render("admin/listUser", {data: data});
    });
});

router.get("/product", function(req,res){
  var getdata = product_md.getAllProduct();

  getdata.then(function(products){
    var data = {
      products : products,
      error : false
    };
    res.render("admin/listProduct", {data: data});
  }).catch(function(err){
    var data = {
      error : "Couldn't get product"
    };
    res.render("admin/listProduct", {data: data});
  });
});

router.delete("/product/delete", function(req,res){
  //lấy id từ web
    var product_masp = req.body.product_masp;
    console.log('masp--------------------------- =' + product_masp);

    var getdata = product_md.deleteProduct(product_masp);

    if(!getdata){
      res.json({status_code: 500});
    }else{
      getdata.then(function(result){
        res.json({status_code: 200});
      }).catch(function (err) {
        res.json({status_code:500});
    });
    }
});

router.get("/product/add", function(req,res){
  res.render("admin/product/addProduct" , {data:{error: false}});
});


// xử lý up hình ảnh


router.post("/product/add", (req, res) => {
  // Thực hiện upload file, truyền vào 2 biến req và res
  uploadFile(req, res, (error) => {
    // Nếu có lỗi thì trả về lỗi cho client.
    // Ví dụ như upload một file không phải file ảnh theo như cấu hình của mình bên trên
    if (error) {
      var data = {
        error :`Error when trying to upload: ${error}`
      };
      console.log("lỗi up hình");
      res.render("admin/product/addProduct", {data: data});
    }

    console.log(`------Request body-----`);
    console.log(req.body);

    console.log(`------Request file-----`);
    console.log(req.file.filename);

    console.log(`------Test Done-----`);
    var params = {
      masp : req.body.masp,
      maloai : req.body.maloai,
      tensp : req.body.tensp,
      dongia : req.body.dongia,
      hinh : req.file.filename,
      mota : req.body.mota,
    }
    console.log(`------params-----`);
    console.log(params);
    if(params.masp.trim().length == 0){
    var data={
      error:"Please enter product id"
    };
    res.render("admin/product/addProduct", {data : data});
  }
  else
  {
    //khởi tạo biến data nhận đối tượng promise 
    //đối tượng promise này lấy params do người dùng nhập váo
    var getdata = product_md.addProduct(params);

    getdata.then(function(result){
      res.redirect("/admin/product");
    }).catch(function(err){
      var data = {
        error :"Couldn't insert posts"
      };
      console.log("lỗi rồi add product");
      res.render("admin/product/addProduct", {data: data});
    });
  }
  });
});


router.get("/product/edit/:masp", function(req,res){
  //lấy data từ trang web
  var params = req.params;
  console.log('Params -------');
  console.log(params);
  var masp = params.masp;
  console.log('Mã sản phẩm -------' + masp);
  //lấy data từ csdl
  var getdata = product_md.getProductById(masp);

  if(getdata){
    getdata.then(function(products){
      var product = products[0];
      var data = {
        product : product,
        error : false,
      };
      res.render("admin/product/editProduct" , {data:data});
    /*lỗi khi mysql chạy*/
    }).catch(function(err){
      var data = {
        error: "My sql err"
      };
      res.render("admin/product/editProduct", {data: data});
    });
  }
  else{
    var data = {
      error: "Couldn't get product by id"
    };
    res.render("admin/product/editProduct", {data: data});
  }
});

router.post("/product/edit", (req, res) => {
  uploadFile2(req, res, (error) => {
    // Nếu có lỗi thì trả về lỗi cho client.
    // Ví dụ như upload một file không phải file ảnh theo như cấu hình của mình bên trên
    if (error) {
      var data = {
        error :`Error when trying to upload: ${error}`
      };
      console.log("lỗi up hình");
      res.render("admin/product/edit", {data: data});
    }
    else{
    console.log(`------Request body-----`);
    console.log(req.body);

    console.log(`------Request file-----`);
    console.log(req.file.filename);

    console.log(`------Test Done-----`);
  }
  var params = {
      masp : req.body.masp,
      maloai : req.body.maloai,
      tensp : req.body.tensp,
      dongia : req.body.dongia,
      hinh : req.file.filename,
      mota : req.body.mota,
  }
  getdata = product_md.updateProduct(params);
  if(!getdata){
    var data = {
      error : 'Lỗi 2'
    };
    res.render("admin/product/edit", {data: data});
  }
  else{
    getdata.then(function(result){
      res.redirect('/admin/product');
    }).catch(function (err) {
      res.send("error");
    });
  }
});
});

module.exports = router;